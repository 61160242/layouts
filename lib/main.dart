import 'package:flutter/material.dart';

void main() {
  // Color c = const Color.fromRGBO(66,165,245,1.0);
  runApp(MaterialApp(
      home: Center(
        child: Container(
          color: Colors.yellow,
          child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        color: Colors.blue,
        height: 50.0,
        width: 50.0,
      ),
      Icon(Icons.adjust, size: 50.0, color: Colors.pink),
      Icon(
        Icons.adjust,
        size: 50.0,
        color: Colors.purple,
      ),
      Icon(
        Icons.adjust,
        size: 50.0,
        color: Colors.greenAccent,
      ),
      Container(
        color: Colors.orange,
        height: 50.0,
        width: 50.0,
      ),
      Icon(
        Icons.adjust,
        size: 50.0,
        color: Colors.cyan,
      ),
    ],
  ),),)
      
  )
);
}
